@Library('groovy-blocks')
import io.rcl.labs.jenkins.libraries.GradlePipeline

node('android') {

  properties([
      buildDiscarder(
          logRotator(
              artifactDaysToKeepStr: '3',
              artifactNumToKeepStr: '8',
              daysToKeepStr: '3',
              numToKeepStr: '8'
          )
      ),
      disableConcurrentBuilds()
  ])

  env.JAVA_OPTS="-Xms1024m -Xmx2048m -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap"
  env.GRADLE_OPTS="-Dorg.gradle.daemon=false -Dkotlin.daemon.jvm.options=\"-Xmx2048m\""

  def buildGradle = GradlePipeline.Builder(this)
      .gradleBuild("clean build")
      .gradleUnitTests([tasks: ["test"])
      .build()

  buildGradle.execute()
}
