@Library('groovy-blocks')
import io.rcl.labs.jenkins.libraries.IosFastlanePipeline

node('macos'){
    def scannerHome = tool 'SonarQube Scanner 3.0.1.733'

    properties([
        buildDiscarder(
            logRotator(
                artifactDaysToKeepStr: '3',
                artifactNumToKeepStr: '8',
                daysToKeepStr: '3',
                numToKeepStr: '8'
            )
        ),
        disableConcurrentBuilds()
    ])

    def iosPipeline = IosFastlanePipeline.Builder(this)
        .context([
            production: [target: "${brand}_prod", schema: "${appName}", project: "${r"${env.WORKSPACE}"}/ios-project/${appName}.xcodeproj"],
            develop: [target: "${brand}_dev", schema: "${appName}", project: "${r"${env.WORKSPACE}"}/ios-project/${appName}.xcodeproj"]
        ])
        .unitTest()
        .build()

    iosPipeline.execute()
}
