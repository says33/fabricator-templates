notification:
  email:
    recipients:
      - ${appName}@rccl.com

  slack:
    channel: devops

ios:
  fastlane:
    certificate: "royal-2018-enterprise"
    provisioningProfile: "${provisionalProfile}"
    provisioningProfileName: "${provisionalProfileName}"
    teamId: "${teamId}"
    appIdentifier: "${appIdentifier}"
    exportMethod: "${exportMethod}"
    productName: "${productName}"
    sdk: "${sdk}"
    devices: "${devices}"
fastlane:
  repository:
    branch: "master"
    url: "ssh://git@bitbucket.rccl.com:7999/devops/fastlane.git"
    credentialsId: "leeroy-jenkins-ssh"
    certificateCredentialsId: "fastlane-ios-credentials"
