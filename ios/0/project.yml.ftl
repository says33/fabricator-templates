name: ${appName}
options:
  bundleIdPrefix: ${bundleIdPrefix}
targets:
  ${brand}_dev:
    type: application
    platform: iOS
    deploymentTarget: "11.0"
    sources: ${brand_source}
    settings:
      configs:
        debug:
          CUSTOM_BUILD_SETTING: ${customBuildSettingDebugValue}
        release:
          CUSTOM_BUILD_SETTING: ${customBuildSettingReleaseValue}
    dependencies:
      - carthage: Alamofire
      - framework: Vendor/MyFramework.framework
      - sdk: Contacts.framework
      - sdk: libc++.tbd
  ${brand}_prod:
    type: application
    platform: iOS
    deploymentTarget: "11.0"
    sources: ${brand_source}
    settings:
      configs:
        debug:
          CUSTOM_BUILD_SETTING: ${customBuildSettingDebugValue}
        release:
          CUSTOM_BUILD_SETTING: ${customBuildSettingReleaseValue}
    dependencies:
      - carthage: Alamofire
      - framework: Vendor/MyFramework.framework
      - sdk: Contacts.framework
      - sdk: libc++.tbd
