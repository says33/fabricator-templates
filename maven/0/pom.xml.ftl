<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.rccl</groupId>
  <artifactId>${appName}</artifactId>
  <packaging>jar</packaging>
  <version>${r"${"}${appName}.version}</version>
  <name>${appName}</name>
  <url>http://maven.apache.org</url>

  <properties>
    <${appName}.version>1.0.0-SNAPSHOT</${appName}.version>
  </properties>

  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
</project>
