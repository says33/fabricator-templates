@Library('groovy-blocks')
import io.rcl.labs.jenkins.libraries.MavenPipeline
import io.rcl.labs.jenkins.libraries.version.MavenPropertyHelper

node {

    env.MAVEN_HOME = "${r"${tool 'Maven 3.3.9'}"}"
    env.JAVA_HOME = "${r"${tool 'jdk8'}"}"
    env.PATH="${r"${env.MAVEN_HOME}/bin:${env.PATH}"}"
    env.PATH="${r"${env.JAVA_HOME}/bin:${env.PATH}"}"

    def getArtifactId = {
      return "${appName}"
    }

    def getFilePath = {
      def mavenVersion = new MavenPropertyHelper(this)
      def version = mavenVersion("pom.xml", "${appName}.version")
      return "target/${appName}-${r"${version}"}.jar"
    }

    properties([
        buildDiscarder(
            logRotator(
                artifactDaysToKeepStr: '3',
                artifactNumToKeepStr: '8',
                daysToKeepStr: '3',
                numToKeepStr: '8'
            )
        ),
        disableConcurrentBuilds()
    ])

    def mavenPipeline = MavenPipeline.Builder(this)
        .addToCommonParams("version", new MavenPropertyHelper(this), ["pom.xml", "${appName}.version"])
        .addToCommonParams("artifactId", getArtifactId, [])
        .addToCommonParams("filePath", getFilePath,[])
        .mavenBuild([tasks: "clean install"])
        .mavenTest([tasks: "test"])
        .build()

    mavenPipeline.execute()
}
