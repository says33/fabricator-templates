@Library('groovy-blocks')
import io.rcl.labs.jenkins.libraries.GradlePipeline

node('mem1-small') {

  properties([
      buildDiscarder(
          logRotator(
              artifactDaysToKeepStr: '3',
              artifactNumToKeepStr: '8',
              daysToKeepStr: '3',
              numToKeepStr: '8'
          )
      ),
      disableConcurrentBuilds()
  ])

  env.JAVA_OPTS="-Xms254m -Xmx512m -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap"

  def buildGradle = GradlePipeline.Builder(this)
      .gradleBuild("clean build")
      .gradleUnitTests([tasks: ["test"])
      .build()

  buildGradle.execute()
}
